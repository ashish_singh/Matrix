#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 18 16:19:44 2018

@author: ashish
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#importing dataset
dataset = pd.read_csv('//Telegram Data//binanceNew.csv')

#splitting messages into a list
import re
dataset['split_messages'] = dataset['Message'].apply(lambda x:((re.sub('[^a-zA-Z]',' ',x)).lower()).split())   

#dropping any messages whose length doesn't meet the criteria
dataset = dataset.drop(dataset[(dataset['split_messages'].apply(lambda x: len(x)) == 0)|(dataset['split_messages'].apply(lambda x: len(x)) > 50)].index)

#UserID of Admins
UserID = [409464881,344347398,314628880,447912744,450657351,422991730,213904490]

#dropping UserID
dataset = dataset.drop(dataset[dataset['UserID'].apply(lambda x: bool(x in UserID)==True)].index)

#creating the matrix
features = ['ico','futures','options','swap','scam','liquidity','dividend','profit','negative','beta','operation','live','launch','spreads','derivatives','leverage','stoploss','trailing','limit','matrix','ladder','license','sec','regulated']
col_names = ['UserID','message_count']+features+['total_count']
matrix = pd.DataFrame(columns = col_names)

grouped = dataset.groupby('UserID')
for name,group in grouped:
    message_count = len(group)
    list_gen = []
    for feature in features:
        list_gen.append(sum(group['split_messages'].apply(lambda x: feature in x)))
    total_count = sum(list_gen)
    list_to = pd.DataFrame([[name,message_count]+list_gen+[total_count]],columns = col_names)
    
    
    matrix = matrix.append(list_to)
    
del(feature,features,group,list_gen,list_to,message_count,name,total_count)
#sorting them as per total message count
matrix = matrix.sort_values(by = 'total_count' , ascending = False)
matrix.to_csv("//Telegram Data//binance_users_new.csv")