#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 24 11:34:07 2018

@author: ashish
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import re

#importing dataset
dataset = pd.read_csv('//home//ashish//Data//binance//binanceNew.csv')
dataset['split_messages'] = dataset['Message'].apply(lambda x:(re.sub('[^a-zA-Z]',' ',x)).lower().split())

#dropping any messages whose length doesn't meet the criteria
dataset = dataset.drop(dataset[(dataset['split_messages'].apply(lambda x: len(x)) == 0)|(dataset['split_messages'].apply(lambda x: len(x)) > 50)].index)

#UserID of Admins
UserID = [409464881,344347398,314628880,447912744,450657351,422991730,213904490]

#dropping UserID
dataset = dataset.drop(dataset[dataset['UserID'].apply(lambda x: bool(x in UserID)==True)].index)

import nltk

from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem import WordNetLemmatizer
wordnet_lemmatizer = WordNetLemmatizer()
questions = ['what','why','how','where','when','who','whom','?']
prep = ['do','can','does','are','is','am']
cols = ['MessNo','UserID','noqs','questions']
dataset['Questions'] = dataset['Message'].apply(lambda x: word_tokenize(x))
dataset['check'] = dataset['Questions'].apply(lambda x:bool(len(set(questions).intersection(x))!=0) or x in prep)
dataset = dataset.drop(dataset[dataset.check == False].index)
dataset['Message'] = dataset['Message'].apply(lambda x:(re.sub('[^a-zA-Z]',' ',x)).lower())

#features = ['ico','futures','options','swap','scam','liquidity','dividend','profit','negative','beta','operation','live','launch','spreads','derivatives','leverage','stoploss','trailing','limit','matrix','ladder','license','sec','regulated']
#dataset['Relevant_Questions'] = dataset['Questions'].apply(lambda x:bool(len(set(features).intersection(x))!=0))
#dataset = dataset.drop(dataset[dataset.Relevant_Questions == False].index)


dataset = dataset.groupby('UserID',as_index = True)['Message'].apply(lambda x:list(x)).reset_index()
dataset['combined'] = dataset['Message'].apply(lambda x:' '.join(x))
dataset = dataset.drop(dataset[(dataset['combined'].apply(lambda x: bool(re.search('how may i help',x) or re.search('how can i help', x))) == True)].index)
dataset['count'] = dataset['Message'].apply(lambda x:len(x))
dataset.drop(['combined'], axis = 1, inplace = True)
dataset = dataset.sort_values(by = 'count' , ascending = False)
dataset.to_csv('//home//ashish//Data//binance//binance_Questions_wadmins.csv')